x = navigator.geolocation;
x.getCurrentPosition(success,failure);

function success(position) {
  
  var myLatitude = position.coords.latitude;
  var myLongitude = position.coords.longitude;

  var coords = new google.maps.LatLng(myLatitude,myLongitude);

  var mapOptions = {

    zoom: 15,
    center: coords,
    mapTypeId: google.maps.MapTypeId.ROADMAP

  }  

  var map = new google.maps.Map(document.getElementById("map"),mapOptions);
  var marker = new google.maps.Marker({map:map,position:coords});

}

function failure(){}