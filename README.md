# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

The code when run, displays the current location of user on google map. 

### How do I get set up? ###

To set up this project on local machine,follow these steps:
1)Clone this repository.
2)Open terminal,type the command $cd geolocation
3)To install the dependencies,run this command- $npm install
4)The last step is to run the server,type the command - $node server.js
5)Now point your browser to localhost:5000 to see the magic.
